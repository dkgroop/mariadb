FROM mariadb:10.3.17-bionic

COPY full.cnf /etc/mysql/my.cnf
RUN chmod 600 /etc/mysql/my.cnf

# COPY my.cnf /etc/mysql/mariadb.conf.d/my.cnf
# RUN chmod 600 /etc/mysql/mariadb.conf.d/my.cnf

